/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chat;

import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author KuchczynskiMarcin000
 */
public class Server {
    ArrayList outputStream;
    
    class ObsluzKlientow implements Runnable
    {
        BufferedReader czytelnik;
        Socket socket;
        public ObsluzKlientow(Socket clientSocket)
        {
            try
            {
                socket=clientSocket;
                InputStreamReader isr= new InputStreamReader(socket.getInputStream());
                czytelnik= new BufferedReader(isr);
                
            }
            catch(IOException ioe)
            {
                System.out.println(ioe);
            }
        }//koniec konstruktora
        @Override
        public void run()
        {
            String wiadomosc;
            try
            {
                while((wiadomosc=czytelnik.readLine())!=null)
                {
                    System.out.println("Odczyt: "+wiadomosc);
                    rozeslij(wiadomosc);
                }
            }
            catch(Exception e){System.out.println(e);}
        }//koniec run
    }//koniec klasy wewnetrzenj
    
    public static void main(String[] args)
    {
        new Server().doIt();
    }
    
    public void doIt()
    {
        outputStream = new ArrayList();
        try
        {
            ServerSocket ss = new ServerSocket(5345);
            while(true)
            {
                Socket clientSocket =ss.accept();
                System.out.println("Port socketa:"+clientSocket.getPort());
                PrintWriter pw = new PrintWriter(clientSocket.getOutputStream());
                outputStream.add(pw);
                
                Thread thread = new Thread(new ObsluzKlientow(clientSocket));
                thread.start();
                System.out.println("Uzyskano połaczenie");
            }
            
        }
        catch(Exception e){System.out.println(e+"Do IT SErvER!");}
    }
    
    public void rozeslij(String message)
    {
        Iterator it = outputStream.iterator();
        while(it.hasNext())
        {
            try
            {
                PrintWriter pisarz = (PrintWriter) it.next();
                pisarz.println(message);
                pisarz.flush();
            }
            catch(Exception e){e.printStackTrace();
                System.out.println("Rozeslij!");}
                
        }
    }
}
