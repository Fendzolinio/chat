/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chat;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.io.*;
import java.net.*;

/**
 *
 * @author KuchczynskiMarcin000
 */
public class Chat {
    JTextArea oknoCzatu;
    JTextField wiadomosc;
    BufferedReader czytelnik;
    PrintWriter pisarz;
    Socket socket;
    JPanel gui;
    JTextField login;
    JButton connect;
    JButton wyslij;
    JScrollPane scroll;
    
    public static void main(String[] args)
    {
        Chat chat = new Chat();
        chat.doIt();
    }
    
    public void doIt()
    {
        JFrame f = new JFrame("Chat");
        f.setContentPane(getGUI());
        f.pack();
        f.setMinimumSize(new Dimension(f.getWidth(),f.getHeight()));
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setVisible(true);
    }
    
    public Container getGUI()
    {
        gui = new JPanel(new BorderLayout(3,3));
        gui.setLayout(new BorderLayout(3,3));
        oknoCzatu = new JTextArea();
        oknoCzatu.setEditable(false);
        oknoCzatu.setWrapStyleWord(true);
        oknoCzatu.setLineWrap(true);
        scroll = new JScrollPane(oknoCzatu);
        scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        scroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        scroll.setPreferredSize(new Dimension(500,300));
//        Dimension d = scroll.getPreferredSize();
//        scroll.setPreferredSize(new Dimension((int)d.getWidth(),(int)d.getHeight()));
        
        
        gui.add(scroll,BorderLayout.CENTER);
        JPanel reszta = new JPanel(new FlowLayout());
        login = new JTextField();
        login.setPreferredSize(new Dimension(150,25));
        reszta.add(login);
        connect = new JButton("Połącz");
        connect.addActionListener(new Polacz());
        reszta.add(connect);
        wiadomosc = new JTextField();
        wiadomosc.setPreferredSize(new Dimension(300,25));
        wiadomosc.addActionListener(new AbstractAction() {
        @Override
        public void actionPerformed(ActionEvent e)
        {
            try
            {
                pisarz.println(login.getText()+": "+wiadomosc.getText());
                pisarz.flush();
            }
            catch(Exception ex)
            {
                System.out.println(ex);
            }
            wiadomosc.setText("");
            wiadomosc.requestFocus();
        }
        }); 
        reszta.add(wiadomosc);
        wyslij = new JButton("Wyślij");
        wyslij.addActionListener(new WyslijListener());
        reszta.add(wyslij);
        gui.add(reszta,BorderLayout.SOUTH);
        
        return gui;
    }
    public void config()
    {
        try
        {
            socket = new Socket("127.0.0.1",5345);
            InputStreamReader czytelnikStrumienia = new InputStreamReader(socket.getInputStream());
            czytelnik = new BufferedReader(czytelnikStrumienia);
            pisarz = new PrintWriter(socket.getOutputStream());
            System.out.println("Obsługa sieci gotowa");
        }
        catch(IOException ioe)
        {
            System.out.println();
        }
    }
    
    class WyslijListener implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e)
        {
            try
            {
                pisarz.println(login.getText()+": "+wiadomosc.getText());
                pisarz.flush();
            }
            catch(Exception ex)
            {
                System.out.println(ex);
            }
            wiadomosc.setText("");
            wiadomosc.requestFocus();
        }
    }
    
    class Polacz implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e)
        {
            try
            {
                if(connect.getText().equals("Połącz"))
                {
                    config();
                    Thread watek = new Thread(new ObslugaKomunikatow());
                    watek.start();
                    pisarz.println("Do pokoju wszedł: "+login.getText().trim());
                    pisarz.flush();
                    login.setEditable(false);
                    connect.setText("Rozłącz");
                    login.setColumns(login.getText().length());
                }
                else
                {
                    connect.setText("Połącz");
                    login.setEditable(true);
                    pisarz.println("Z pokoju wyszedł: "+login.getText().trim());
                    pisarz.flush();
                    socket.close();
                    login.requestFocus();
                    login.setPreferredSize(new Dimension(150,25));
                    
                }
            }
            catch(Exception ioe)
            {
                System.out.println(ioe);
            }
        }
    }
    
    class ObslugaKomunikatow implements Runnable
    {
        @Override
        public void run()
        {
            String wiadom;
            try
            {
                while((wiadom = czytelnik.readLine())!=null)
                {
                    
                    System.out.println("Odczytano: "+wiadom);
                    oknoCzatu.append(wiadom+"\n");
                }
                
            }
            catch(Exception e){System.out.println(e);}
        }
    }
}